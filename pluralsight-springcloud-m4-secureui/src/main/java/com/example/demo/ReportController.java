package com.example.demo;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@EnableOAuth2Sso
public class ReportController extends WebSecurityConfigurerAdapter {

	@Autowired
	private OAuth2ClientContext clientContext;

	@Autowired
	private OAuth2RestTemplate oAuth2RestTemplate;

	@GetMapping("/")
	public String loadHome() {
		return "home";
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers("/", "/login**").permitAll().anyRequest().authenticated();
	}

	@GetMapping("/reports")
	public String loadReport(Model model) {

		OAuth2AccessToken t = clientContext.getAccessToken();
		System.out.println(t.getValue());

		ResponseEntity<ArrayList<TollUsage>> tolls = oAuth2RestTemplate.exchange(
				"http://localhost:9001/services/tolldata", HttpMethod.GET, null,
				new ParameterizedTypeReference<ArrayList<TollUsage>>() {
				});

		model.addAttribute("tolls", tolls.getBody());

		return "reports";

	}

	public static class TollUsage {

		public String Id;
		public String stationId;
		public String licensePlate;
		public String timeStamp;

		public TollUsage() {
			// TODO Auto-generated constructor stub
		}

		public TollUsage(String id, String stationId, String licensePlate, String timeStamp) {
			super();
			Id = id;
			this.stationId = stationId;
			this.licensePlate = licensePlate;
			this.timeStamp = timeStamp;
		}

	}

}
