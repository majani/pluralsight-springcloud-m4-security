package com.example.demo;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableResourceServer
public class PluralsightSpringcloudM4SecureuiserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PluralsightSpringcloudM4SecureuiserviceApplication.class, args);
	}

	@GetMapping("/tolldata")
	public ArrayList<TollUsage> getTollData() {

		TollUsage instance1 = new TollUsage("100", "station150", "BGT456", "2020-01-02:31:22");
		TollUsage instance2 = new TollUsage("101", "station150", "RTY433", "2020-01-02:31:22");
		TollUsage instance3 = new TollUsage("102", "station150", "GHJ324", "2020-01-02:31:22");
		TollUsage instance4 = new TollUsage("103", "station150", "SDF453", "2020-01-02:31:22");

		ArrayList<TollUsage> tolls = new ArrayList<>();
		tolls.add(instance1);
		tolls.add(instance2);
		tolls.add(instance3);
		tolls.add(instance4);

		return tolls;
	}

}
