package com.example.demo;

public class TollUsage {

	public String Id;
	public String stationId;
	public String licensePlate;
	public String timeStamp;

	public TollUsage(String id, String stationId, String licensePlate, String timeStamp) {
		super();
		Id = id;
		this.stationId = stationId;
		this.licensePlate = licensePlate;
		this.timeStamp = timeStamp;
	}

}
